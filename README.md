(function($){
    $.ASClient.HTTPHost = '';
    $.Application = {
        id: '',
        mode: 1,
        codes: {
            BL_QC: '11',
            BL_DEMONT: '12',
            BL_MONT: '13',
            BL_REPAIR: '14',
            L_PREPARE: '11',
            L_QC: '12',
            L_TECH: '13',
            L_QUIT: '14',
            L_ENTER: '15',
            L_CLEAN: '16',
            RCP_START: '01',
            RCP_STOP: '02',
            RCP_WAITING: '30',
            RCP_PAUSE: '35',
            SYS_FIX: '28',
            SYS_REPAIR_DONE: '20'
        }
    };
})(window);
