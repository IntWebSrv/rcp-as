(function(){
    var soap = require('soap');
    var errorCodes = {
        "ERROR_SAME_CODE": "Odbicie zostało już wykonane, czas: ",
        "ERROR_NO_EXIT": "Musisz najpierw wyjść z pracy ;)",
        "ERROR_NO_ENTER": "Musisz najpierw wejść do pracy ;)",
        "ERROR_WORK_TIME_TO_LONG": "Zbyt długi czas pracy"
    };
    window.ASClient = {};
    window.ASClient.HTTPHost = 'http://195.168.1.89/SV_RCP/RcpWebService/WebService.asmx?wsdl';
    window.ASClient.WriteEvent = function(operation, user, zlec, onSend) {
        soap.createClient(window.ASClient.HTTPHost, {
            forceSoap12Headers: true
        }, function(err, client) {
            var input = {
                pOperationCode: operation,
                pUserId: user
            };
            if (zlec) {
                input.pZlecDocId = zlec;
            }
            client.WriteEvent(input, function(err, result, requestRaw) {
                console.log(JSON.stringify(err), JSON.stringify(result), JSON.stringify(input), JSON.stringify(requestRaw));
                if (err == null && result == null) {
                    window.runtime.showMessage('Błąd bazy danych', 'error');
                }
                if (result.WriteEventResult.diffgram.NewDataSet.RCP_EVENT) {
                    if (onSend) {
                        onSend(result.WriteEventResult.diffgram.NewDataSet.RCP_EVENT);
                    }
                    console.log(JSON.stringify(result.WriteEventResult.diffgram.NewDataSet.RCP_EVENT));
                } else {
                    var message = result.WriteEventResult.diffgram.NewDataSet.ERROR.CODE;
                    switch(message) {
                        case "ERROR_SAME_CODE":
                            var timeString = "", timeInSec = result.WriteEventResult.diffgram.NewDataSet.ERROR.TIME,
                                secs = timeInSec % 60,
                                min = Math.floor(((timeInSec - secs) % 3600) / 60),
                                hrs = Math.floor((timeInSec - secs - min * 60) / 3600);
                            message = errorCodes.ERROR_SAME_CODE + hrs +':'+ (min < 10 ? ('0' + min.toString()) : min) + ':' + (secs < 10 ? ('0' + secs.toString()) : secs);
                            break;
                        default:
                            if (errorCodes.hasOwnProperty(message)) {
                                message = errorCodes[message];
                            }
                    }
                    window.runtime.showMessage('Błąd zapisu zdarzenia: '+ message, 'error');
                }
            })
        });
    };
    window.ASClient.GetUserRowByCardNumber = function(id, onFinish) {
        soap.createClient(window.ASClient.HTTPHost, {
            forceSoap12Headers: true
        }, function(err, client) {
            client.GetUserRowByCardNumber({
                pIdExt: id
            }, function(err, result) {
                if (result.GetUserRowByCardNumberResult.diffgram.NewDataSet) {
                    onFinish({
                        id: result.GetUserRowByCardNumberResult.diffgram.NewDataSet.RCP_USER.RCP_USER_ID,
                        firstName: result.GetUserRowByCardNumberResult.diffgram.NewDataSet.RCP_USER.IMIE,
                        lastName: result.GetUserRowByCardNumberResult.diffgram.NewDataSet.RCP_USER.NAZWISKO,
                        role: result.GetUserRowByCardNumberResult.diffgram.NewDataSet.RCP_USER.FUNKCJA_KOD
                    });
                    console.log('Pobrano informacje o pracowniku');
                }
                // onFinish(null);
            })
        });
    };
    window.ASClient.GetZlecDocRowByBarcode = function(id, onFinish) {
        soap.createClient(window.ASClient.HTTPHost, {
            forceSoap12Headers: true
        }, function(err, client) {
            client.GetZlecDocRowByBarcode({
                pBarcodeNumber: id
            }, function(err, result) {
                if (result.GetZlecDocRowByBarcodeResult.diffgram) {
                    console.log(JSON.stringify(result.GetZlecDocRowByBarcodeResult.diffgram.NewDataSet.RCP_ZLECDOC));
                    onFinish({
                        id: result.GetZlecDocRowByBarcodeResult.diffgram.NewDataSet.RCP_ZLECDOC.ID_EXT,
                        status: result.GetZlecDocRowByBarcodeResult.diffgram.NewDataSet.RCP_ZLECDOC.STATUS_KOD,
                        VIN: result.GetZlecDocRowByBarcodeResult.diffgram.NewDataSet.RCP_ZLECDOC.NR_NADWOZIA,
                        licensePlate: result.GetZlecDocRowByBarcodeResult.diffgram.NewDataSet.RCP_ZLECDOC.NR_REJESTRACYJNY,
                        car: result.GetZlecDocRowByBarcodeResult.diffgram.NewDataSet.RCP_ZLECDOC.MMT_OPIS,
                        description: result.GetZlecDocRowByBarcodeResult.diffgram.NewDataSet.RCP_ZLECDOC.OPIS,
                        readable: result.GetZlecDocRowByBarcodeResult.diffgram.NewDataSet.RCP_ZLECDOC.NUMER,
                        rcp_id: result.GetZlecDocRowByBarcodeResult.diffgram.NewDataSet.RCP_ZLECDOC.RCP_ZLECDOC_ID
                    });
                    return ;
                } else {
                    console.log(JSON.stringify(result.GetZlecDocRowByBarcodeResult));
                }
                onFinish(null);
            })
        });
    };
    window.ASClient.GetBarCodeConfig = function(onFinish) {
        soap.createClient(window.ASClient.HTTPHost, {
            forceSoap12Headers: true
        }, function(err, client) {
            client.GetBarCodeConfig({}, function(err, result) {
                var items = {};
                result.GetBarCodeConfigResult.diffgram.NewDataSet.RCP_BARCODES_CONFIG.forEach(function(item){
                    // console.log(item);
                    items[item.KOD] = item.PREFIX;
                });
                console.log('Pobrano liste kodow skanera');
                onFinish(items);
            })
        });
    };
    window.ASClient.GetOperationsCodeConfig = function(onFinish){
        soap.createClient(window.ASClient.HTTPHost, {
            forceSoap12Headers: true
        }, function(err, client) {
            client.GetOperationsCodeConfig({}, function(err, result) {
                var items = [];
                result.GetOperationsCodeConfigResult.diffgram.NewDataSet.RCP_OPERATIONS_CONFIG.forEach(function(item, k){
                    // console.log(JSON.stringify(item));
                    items[parseInt(item.attributes['msdata:rowOrder'])] = {
                        config_id: item.RCP_OPERATIONS_CONFIG_ID,
                        code: item.KOD,
                        name: item.NAZWA,
                        require_zlecdoc: item.F_ZLECDOC_REQUIRED == "1",
                        F_ENTER: !!item.F_ENTER,
                        F_EXIT: !!item.F_EXIT,
                        F_CLOSE_ZLECDOC: !!item.F_CLOSE_ZLECDOC,
                        CLOSE_ZLECDOC_NEXT_CODE: parseInt(item.CLOSE_ZLECDOC_NEXT_CODE),
                        auto_user: item.F_DEF == "1",
                        F_RCP_DISPLAY: !!item.F_RCP_DISPLAY,
                        F_ZLECDOC_REQUIRED: item.F_ZLECDOC_REQUIRED,
                        auto_task: item.ZLECDOC_NUMER_PATTERN != undefined
                    };
                });
                console.log('Pobrano liste kodow operacji');
                onFinish(items);
            });
        });
    };
})();