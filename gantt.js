(function ($, $$, ce) {
    "use strict";
    $$.gantt = function (target) {
        var workshops = [];
        var damage_id;
        var items = [];
        var legend = [];
        var scale = 150;
        var instance = this;
        var onItemClick = function (e) {
            console.log('click on item with ID', e.target.dataset.id);
            // create modal
            var fade = $[ce]('div'),
                dialog = $[ce]('div'),
                content = $[ce]('form'),
                header = $[ce]('div'),
                body = $[ce]('div'),
                footer = $[ce]('div'),
                title = $[ce]('h4');
            // set classes
            fade.className = 'modal fade';
            dialog.className = 'modal-dialog';
            content.className = 'modal-content';
            header.className = 'modal-header';
            body.className = 'modal-body';
            footer.className = 'modal-footer';
            title.className = 'modal-title';
            // other info
            title.textContent = e.target.dataset.name;
            // modal content
            var itmId = $[ce]('input');
            itmId.setAttribute('name', 'item_id');
            itmId.setAttribute('type', 'hidden');
            itmId.setAttribute('value', e.target.dataset.id);
            body.appendChild(itmId);
            var fgStart = $[ce]('div'), fgStartLabel = $[ce]('label'), fgStartInput = $[ce]('input');
            fgStartLabel.className = 'control-label';
            fgStartLabel.textContent = 'Data rozpoczęcia:';
            fgStart.appendChild(fgStartLabel);
            fgStartInput.className = 'form-control';
            fgStartInput.setAttribute('name', 'begin');
            fgStartInput.setAttribute('type', 'datetime');
            fgStartInput.setAttribute('value', e.target.dataset.begin);
            fgStart.appendChild(fgStartInput);
            fgStart.className = 'form-group';
            body.appendChild(fgStart);
            /*var fgFinish = $[ce]('div'), fgFinishLabel = $[ce]('label'), fgFinishInput = $[ce]('input');
             fgFinishLabel.className = 'control-label';
             fgFinishLabel.textContent = 'Data zakończenia:';
             fgFinish.appendChild(fgFinishLabel);
             fgFinishInput.className = 'form-control';
             fgFinishInput.setAttribute('name', 'finish');
             fgFinishInput.setAttribute('type', 'datetime');
             fgFinish.appendChild(fgFinishInput);
             fgFinish.className = 'form-group';
             body.appendChild(fgFinish);*/
            var fgWorker = $[ce]('div'), fgWorkerLabel = $[ce]('label'), fgWorkerSelect = $[ce]('select');
            fgWorkerLabel.className = 'control-label';
            fgWorkerLabel.textContent = 'Pracownik:';
            fgWorkerSelect.className = 'form-control';
            fgWorkerSelect.setAttribute('name', 'worker_id');
            fgWorker.appendChild(fgWorkerLabel);
            fgWorker.appendChild(fgWorkerSelect);
            workshops[e.target.dataset.workshopId].workers.forEach(function (worker) {
                var option = $[ce]('option');
                option.setAttribute('value', worker.id);
                option.textContent = worker.name;
                fgWorkerSelect.appendChild(option);
            });
            body.appendChild(fgWorker);
            // submit button
            var btn = $[ce]('button');
            btn.className = 'btn btn-primary';
            btn.textContent = 'Zapisz';
            btn.setAttribute('type', 'submit');
            var fn = 'addEventListener', act = 'click';
            if (!btn.addEventListener) {
                fn = 'attachEvent';
                act = 'onclick'
            }
            btn[fn](act, function () {
                alert('Zapisywanie zmian');
            });
            footer.appendChild(btn);
            // append to view
            fade.appendChild(dialog);
            dialog.appendChild(content);
            content.appendChild(header);
            header.appendChild(title);
            content.appendChild(body);
            content.appendChild(footer);
            target.appendChild(fade);
            var jQModal = window.jQuery(fade);
            jQModal.modal('show');
            jQModal.on('hidden.bs.modal', function (e) {
                target.removeChild(fade);
            });
            window.jQuery(content).on('submit', function (e) {
                var _t = window.jQuery(this);
                e.preventDefault();
                window.jQuery.ajax({
                    url: '/ajax/ganttup',
                    data: _t.serialize(),
                    type: 'POST',
                    success: function () {
                        window.jQuery.ajax({
                            url: '/ajax/ganttsource',
                            data: {damage_id: damage_id},
                            type: 'POST',
                            success: function (jsonData) {
                                instance.import(jsonData);
                                instance.draw();
                                jQModal.modal('hide');
                            }
                        });
                    }
                });
            });
        };
        target.style.fontSize = '11px';
        this.setScale = function (nS) {
            scale = parseInt(nS);
            return this;
        };
        this.import = function (dataset) {
            console.info('Importing...');
            // damage_id = dataset.damage_id;
            workshops = [];
            items = [];
            if (Array.isArray(dataset.workshops)) {
                dataset.workshops.forEach(function (item) {
                    workshops[item.id] = item;
                    item.workers.forEach(function (worker) {
                        worker.groups.forEach(function (group) {
                            items.push(group);
                        });
                    });
                });
            }
            console.info(workshops.length, 'workshop imported');
            console.info(items.length, 'item imported');
        };
        this.draw = function () {
            if (scale >= 480) {
                scale = 480;
            } else if (scale >= 240) {
                scale = 240;
            } else if (scale >= 120) {
                scale = 120;
            } else {
                scale = 60;
            }
            target.innerHTML = '';
            var workers = [];
            var masterDiv = $[ce]('div');
            masterDiv.style.width = '100%';
            masterDiv.style.height = '300px';
            masterDiv.style.overflowY = 'auto';
            masterDiv.style.position = 'relative';
            target.appendChild(masterDiv);
            var workersDiv = $[ce]('div');
            workersDiv.className = 'workers';
            masterDiv.appendChild(workersDiv);
            var itemsDiv = $[ce]('div');
            itemsDiv.className = 'items';
            masterDiv.appendChild(itemsDiv);
            // podziałka dzienna
            var divDays = $[ce]('div');
            divDays.className = 'days';
            itemsDiv.appendChild(divDays);
            // podziałka godzinowa
            var divHours = $[ce]('div');
            divHours.className = 'hours';
            itemsDiv.appendChild(divHours);
            // gantt height
            var gantt_height = 100;
            // append workers
            workshops.forEach(function (workshop) {
                workshop.workers.forEach(function (worker) {
                    gantt_height += 30;
                    var divName = $[ce]('div');
                    divName.style.height = '30px';
                    divName.style.width = '100%';
                    divName.textContent = worker.name;
                    workersDiv.appendChild(divName);
                    var divItems = $[ce]('div');
                    divItems.style.height = '30px';
                    divItems.style.width = '1px';
                    divItems.style.minWidth = '100%';
                    divItems.style.position = 'relative';
                    divItems.style.backgroundImage = 'linear-gradient(#dadada 1px, transparent 1px),' +
                        'linear-gradient(90deg, #c7c7c7 1px, transparent 1px),' +
                        'linear-gradient(transparent 1px, transparent 1px),' +
                        'linear-gradient(90deg, #dadada 1px, transparent 1px)';
                    divItems.style.backgroundSize = '60px 30px, 60px 60px, 15px 15px, 15px 15px';
                    itemsDiv.appendChild(divItems);
                    workers[worker.id] = divItems;
                });
            });
            masterDiv.style.height = gantt_height + 'px';
            // append items to list
            // TODO wsparcie skali
            var color_mapping = [];
            legend = [];
            items.forEach(function (item) {
                if (!item.important) {
                    var color_id = color_mapping.indexOf(item.damage_id);
                    if (color_id == -1) {
                        color_id = color_mapping.length;
                        color_mapping.push(item.damage_id);
                        legend.push(item.damage_info);
                    }
                }

                //console.log('Showing', item, 'item');
                var div = $[ce]('div');
                div.className = 'item item-'+item.type + ' item-damage'+color_id;
                if(item.name != '') {
                    div.title = item.name;
                    div.className += ' pop tip';
                    div.dataset.title = item.damage_title;
                    div.dataset.content = item.details;
                }
                if(item.important) {
                    div.className += ' important';
                }
                div.style.left = Math.round(item.start / scale).toString() + 'px';
                div.style.width = Math.round((item.stop - item.start) / scale).toString() + 'px';
                //div.textContent = item.name;
                var targetWidth = (item.stop / scale);
                if (parseInt(workers[item.user_id].style.width) < targetWidth) {
                    workers.forEach(function (div) {
                        divHours.style.width = div.style.width = targetWidth.toString() + 'px';
                    });
                }
                workers[item.user_id].appendChild(div);
                //console.log(div.style);
                div.dataset.id = item.id;
                div.dataset.begin = item.begin;
                div.dataset.finish = item.finish;
                div.dataset.workshopId = item.workshopId;
                if (item.editable) {
                    div.style.cursor = 'pointer';
                    // append onclick action
                    if (div.addEventListener) {
                        div.addEventListener('click', onItemClick);
                    } else {
                        div.attachEvent('onclick', onItemClick);
                    }
                } else {
                    div.style.cursor = 'default';
                }
            });
            var gantt_legend = document.getElementById('gantt_legend');
            gantt_legend.innerHTML = '';
            if (gantt_legend) {
                legend.forEach(function (damage_info, color_id) {
                    if (damage_info != '') {
                        var div = $[ce]('div');
                        div.className = 'legend-' + color_id;
                        div.innerHTML = damage_info;
                        gantt_legend.appendChild(div);
                    }
                });
            }
            var newWidth = parseInt(divHours.style.width) + (86400 / scale);
            divDays.style.width = divHours.style.width = newWidth.toString() + 'px';
            workers.forEach(function (wdiv) {
                wdiv.style.width = divHours.style.width;
            });
            var itr, stepDate = new Date();
            var daysCount = Math.floor(newWidth / 1440);
            for (itr = 0; itr < daysCount; itr++) {
                var div = $[ce]('div');
                div.style.height = '30px';
                div.textContent = stepDate.toISOString().slice(0, 10);
                switch (scale) {
                    case 60:
                        div.style.width = '1440px';
                        div.style.left = (1440 * itr).toString() + 'px';
                        stepDate.setTime(stepDate.getTime() + 86400000);
                        break;
                    case 120:
                        div.style.width = '720px';
                        div.style.left = (720 * itr).toString() + 'px';
                        stepDate.setTime(stepDate.getTime() + 43200000);
                        break;
                    case 240:
                        div.style.width = '10080px';
                        div.style.left = (10080 * itr).toString() + 'px';
                        stepDate.setTime(stepDate.getTime() + 604800000);
                        break;
                    case 480:
                        div.style.width = '5040px';
                        div.style.left = (5040 * itr).toString() + 'px';
                        stepDate.setTime(stepDate.getTime() + 302400000);
                        break;
                }
                div.style.position = 'absolute';
                div.style.top = 0;
                div.style.textAlign = 'center';
                divDays.appendChild(div);
            }
            var hoursCount = Math.floor(newWidth / scale);
            for (itr = 0; itr < hoursCount; itr++) {
                var div = $[ce]('div');
                div.style.height = '50px';
                div.style.position = 'absolute';
                div.style.top = 20;
                div.style.textAlign = 'center';
                var hr = $[ce]('span');
                hr.style.float = 'left';
                hr.style.fontSize = '15px';
                hr.style.height = '30px';
                hr.style.lineHeight = '30px';
                hr.innerHTML = (itr % 24).toString();
                div.appendChild(hr);
                var steps = [0, 15, 30, 45], blockWidth = 60;
                switch (scale) {
                    case 60:
                        break;
                    case 120:
                        blockWidth = 30;
                        steps = [0, 30];
                        break;
                    case 240:
                        div.style.background = '#fff';
                        blockWidth = 360;
                        hr.innerHTML += '<sup> 00</sup>';
                        steps = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
                        break;
                    case 480:
                        blockWidth = 180;
                        steps = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22];
                        break;
                }
                div.style.width = (blockWidth + 1).toString() + 'px';
                div.style.left = (blockWidth * itr).toString() + 'px';
                var stepSize = Math.floor(blockWidth / steps.length).toString() + 'px';
                hr.style.width = div.style.width;
                steps.forEach(function (step) {
                    var min = $[ce]('span');
                    min.style.width = stepSize;
                    min.style.float = 'left';
                    min.style.height = '20px';
                    min.style.lineHeight = '20px';
                    min.style.fontSize = '8px';
                    min.textContent = step;
                    div.appendChild(min);
                });
                divHours.appendChild(div);
            }
        };
    };
})(window.document, window, 'createElement');
