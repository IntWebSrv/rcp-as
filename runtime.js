(function ($) {
    // application config
    var APPLICATION_WS = 'http://dev.dealerbms.pl/restcs/';
    var RCP_PROTO_VER = '2';
    //var APPLICATION_WS = 'http://localhost:1340/restcs/';
    // BARCODE scanner
    var input_stock = [], sharp = false, bar = false, scanner = false, command_stock = [], commandQueue = [];
    // current status
    var user, damage, command, userInfo = $.document.getElementById('user-info'), lastSignal, autoTask, autoUser;
    // autostacja config
    var AS_PREFIX = {
        ZLECDOC: 'DL^',
        USER_A: 'USER_A_'
    }, CommandList = [];
    // schedule
    var gantt;
    // application code
    $.runtime = {
        activeTab: 'report',
        parseBarCode: function (input) {
            if (input.toLowerCase().indexOf(AS_PREFIX.ZLECDOC.toLowerCase()) === 0) {
                // read information from AS
                window.ASClient.GetZlecDocRowByBarcode(input, function (result) {
                    if (result == null) {
                        $.runtime.showMessage('Brak zlecenia w systemie.');
                        return;
                    }
                    damage = result;
                    $.runtime.sendCommandIfAllowed();
                });
            } else if (input.toLowerCase().indexOf(AS_PREFIX.USER_A.toLowerCase()) === 0) {
                input = input.substr(AS_PREFIX.USER_A.length);
                // read information from AS
                window.ASClient.GetUserRowByCardNumber(input, function (result) {
                    if (result == null) {
                        $.runtime.showMessage('Taki użytkownik nie widnieje w systemie.');
                        return;
                    }
                    user = result;
                    $.runtime.sendCommandIfAllowed();
                });
            }
        },
        resetState: function (no_reset_view) {
            command = null;
            user = null;
            damage = null;
            sharp = bar = false;
            input_stock = [];
            $.document.getElementById('current-command').textContent = "--";
            $.document.getElementById('sharpState').className = '';
            $.document.getElementById('barState').className = '';
            lastSignal = (new Date()).getTime();
        },
        sendCommand: function (code, userId, damageId, over_damage_id) {
            $.ASClient.WriteEvent(code, userId, damageId, function(data){
                if (data.RCP_ZLECDOC_ID) {
                    data.RCP_ZLECDOC_ID = over_damage_id;
                }
                window.runtime.showMessage('Zdarzenie "'+ command.name +'" zostało zapisane', 'success');
                commandQueue.push(JSON.stringify(data));
                $.runtime.resetState();
            });
        },
        sendCommandIfAllowed: function () {
            // reset view
            sharp = bar = false;
            var dmgEls = $.document.querySelectorAll('[data-damage]'), itr = 0;
            for (; itr < dmgEls.length; itr++) {
                dmgEls.item(itr).textContent = '-';
            }
            userInfo.textContent = '-';
            $.document.getElementById('current-command').textContent = "--";
            if (command) {
                $.document.getElementById('current-command').textContent = command.code;
            }
            if (damage) {
                for (itr= 0; itr < dmgEls.length; itr++) {
                    dmgEls.item(itr).textContent = damage[dmgEls.item(itr).dataset.damage];
                }
            }
            if (user) {
                userInfo.textContent = user.firstName + ' ' + user.lastName + ' (' + user.role + ') [' + user.id + ']';
            }
            lastSignal = (new Date()).getTime();
			$.document.getElementById('information').style.display = 'none';
            if (!command && user && !damage) {
                command = autoUser;
            } else if (!command && user && damage) {
                command = autoTask;
            }
            if (command && command.require_zlecdoc && damage && user) {
                $.runtime.sendCommand(command.code, user.id, damage.rcp_id, damage.id);
            } else if (command && !command.require_zlecdoc && user) {
                $.runtime.sendCommand(command.code, user.id, null);
            } else if ($.runtime.activeTab == 'my-jobs' && user) {
                var box = $.document.getElementById('my-jobs');
                box.innerHTML = 'TRWA ŁADOWANIE DANYCH';
                var request = new XMLHttpRequest();
                request.open('POST', APPLICATION_WS + 'jobs');
                request.setRequestHeader('Application-ID', $.Application.id);
                request.setRequestHeader('RCP-Proto-Ver', RCP_PROTO_VER);
                request.onload = function(e){
                    var response = JSON.parse(e.target.responseText);
                    if (response.length > 0) {
                        var zlecList = $.document.createElement('ul');
                        zlecList.className = 'errands';
                        response.forEach(function (group) {
                            var errandItem = $.document.createElement('li');
                            errandItem.textContent = group.errand.readable;
                            errandItem.setAttribute('title', group.items.join(', '));
                            zlecList.appendChild(errandItem);
                        });
                        box.innerHTML = '';
                        box.appendChild(zlecList);
                    } else {
                        box.innerHTML = '';
                        $.runtime.showMessage('Brak oczekujących zleceń. Gratuluję!', 'success');
                    }
                };
                request.send(user.id);
            }
        },
        showMessage: function(message, className){
            if (className == undefined) {
                className = 'error';
            }
            var infoBox = $.document.getElementById('information');
            infoBox.textContent = message;
            infoBox.style.display = 'block';
            infoBox.className = className;
            $.runtime.resetState();
        },
        updateGantt: function(){
            var request = new XMLHttpRequest;
            request.open('GET', APPLICATION_WS + 'schedule');
            request.setRequestHeader('Application-ID', $.Application.id);
            request.setRequestHeader('RCP-Proto-Ver', RCP_PROTO_VER);
            request.onload = function(e){
                gantt.import(JSON.parse(e.target.responseText));
                gantt.draw();
            };
            request.send();
        }
    };
    $.addEventListener('keypress', function (e) {
        e.preventDefault();
        switch (e.which) {
            case 35:
                sharp = !sharp;
                $.document.getElementById('sharpState').className = sharp ? 'active' : '';
                break;
            case 124:
                bar = !bar;
                $.document.getElementById('barState').className = bar ? 'active' : '';
                break;
            default:
                scanner = sharp && bar;
                if (scanner) {
                    input_stock.push(String.fromCharCode(e.which));
                } else {
                    if (e.which >= 48 && e.which <= 57 && !bar && !sharp) {
                        command_stock.push(parseInt(e.which - 48).toString());
                    }
                }
                break;
        }
        scanner = sharp && bar;
        if (input_stock.length > 0 && !scanner) {
            $.runtime.parseBarCode(input_stock.join(''));
            input_stock = [];
        } else if (!scanner && command_stock.length >= 2) {
            // search for command
            var ct = command_stock[command_stock.length - 2] + command_stock[command_stock.length - 1];
            command_stock = [];
            //
            if (ct == "00") {
                $.document.getElementById('current-command').textContent = "00";
                command = null;
            }
            var cc = CommandList.map(function (cmd) {
                return cmd.code
            }).indexOf(ct);
            if (cc != -1) {
                command = CommandList[cc];
                $.document.getElementById('current-command').textContent = ct;
                $.runtime.sendCommandIfAllowed();
            }
        }
    });
    $.document.getElementById('reset-app').addEventListener('click', function () {
        // alert('RESET');
        $.runtime.resetState();
        $.runtime.sendCommandIfAllowed();
    });
    $.ASClient.GetBarCodeConfig(function (response) {
        AS_PREFIX = response;
    });
    if (($.Application.mode & 1) == 1) {
        $.ASClient.GetOperationsCodeConfig(function (response) {
            response.forEach(function (opt) {
                CommandList.push(opt);
                var btn = $.document.createElement('button');
                btn.className = 'btn btn-default btn-block';
                btn.dataset.command = opt.code;
                btn.textContent = opt.name + ' (' + opt.code + ')';
                if (opt.auto_user) {
                    autoUser = opt;
                    btn.textContent += ' (zadanie automatyczne)';
                }
                if (opt.auto_task) {
                    autoTask = opt;
                    btn.textContent += ' (naprawa automatyczna)';
                }
                // 26
                btn.addEventListener('click', function (e) {
                    var cc = CommandList.map(function (cmd) {
                        return cmd.code
                    }).indexOf(e.target.dataset.command);
                    if (cc != -1) {
                        command = CommandList[cc];
                        $.document.getElementById('current-command').textContent = e.target.dataset.command;
                        $.runtime.sendCommandIfAllowed();
                    }
                });
                $.document.getElementById('application-sidebar').appendChild(btn);
            });
        });
    }
    // BL
    if (($.Application.mode & 6) > 0 && ($.Application.mode & 1) == 0) {
        //
        [
            {code: $.Application.codes.RCP_START,name: 'Wejście do pracy',auto_user: false,auto_task: false,require_zlecdoc:false},
            {code: $.Application.codes.RCP_STOP,name: 'Wyjście z pracy',auto_user: false,auto_task: false,require_zlecdoc:false},
            {code: $.Application.codes.RCP_WAITING,name: 'Oczekiwanie na zlecenie',auto_user: true,auto_task: false,require_zlecdoc:false},
            {code: $.Application.codes.RCP_PAUSE,name: 'Przerwa',auto_user: false,auto_task: false,require_zlecdoc:false}
        ].forEach(function(opt){
            CommandList.push(opt);
        });
    }
    if (($.Application.mode & 2) == 2) {
        [
            {code: $.Application.codes.BL_DEMONT,name: 'Demontaż',auto_user: false,auto_task: false,require_zlecdoc:true},
            {code: $.Application.codes.BL_REPAIR,name: 'Naprawa',auto_user: false,auto_task: false,require_zlecdoc:true},
            {code: $.Application.codes.BL_MONT,name: 'Montaż',auto_user: false,auto_task: false,require_zlecdoc:true},
            {code: $.Application.codes.BL_QC,name: 'Kontrola jakości',auto_user: false,auto_task: false,require_zlecdoc:true}
        ].forEach(function(opt){
            CommandList.push(opt);
        });
    }
    // L
    if (($.Application.mode & 4) == 4) {
        if ($.Application.codes['L_POLISH'] == undefined) {
            $.Application.codes.L_POLISH = '17';
        }
        [
            {code: $.Application.codes.L_PREPARE,name: 'Przygotowanie',auto_user: false,auto_task: false,require_zlecdoc:true},
            {code: $.Application.codes.L_CLEAN,name: 'Strefa czysta',auto_user: false,auto_task: false,require_zlecdoc:true},
            {code: $.Application.codes.L_ENTER,name: 'Naprawa lakiernicza',auto_user: false,auto_task: false,require_zlecdoc:true},
            {code: $.Application.codes.L_QUIT,name: 'Wyjście z kabiny',auto_user: false,auto_task: false,require_zlecdoc:true},
            {code: $.Application.codes.L_TECH,name: 'Czas technologiczny',auto_user: false,auto_task: false,require_zlecdoc:true},
            {code: $.Application.codes.L_POLISH,name: 'Polerowanie',auto_user: false,auto_task: false,require_zlecdoc:true},
            {code: $.Application.codes.L_QC,name: 'Kontrola jakości',auto_user: false,auto_task: false,require_zlecdoc:true}
        ].forEach(function(opt){
            CommandList.push(opt);
        });
    }
    if (($.Application.mode & 6) > 0) {
        CommandList.push({
            code: $.Application.codes.SYS_FIX,
            name: 'Poprawki',
            auto_user: false,
            auto_task: false,
            require_zlecdoc: true
        });
        CommandList.push({
            code: $.Application.codes.SYS_REPAIR_DONE,
            name: 'Końcowa kontrola jakości',
            auto_user: false,
            auto_task: false,
            require_zlecdoc: true
        });
    }
    CommandList.forEach(function (opt) {
        var btn = $.document.createElement('button');
        btn.className = 'btn btn-default btn-block';
        btn.dataset.command = opt.code;
        btn.textContent = opt.name + ' (' + opt.code + ')';
        if (opt.auto_user) {
            autoUser = opt;
            btn.textContent += ' (zadanie automatyczne)';
        }
        if (opt.auto_task) {
            autoTask = opt;
            btn.textContent += ' (naprawa automatyczna)';
        }
        // 26
        btn.addEventListener('click', function (e) {
            var cc = CommandList.map(function (cmd) {
                return cmd.code
            }).indexOf(e.target.dataset.command);
            if (cc != -1) {
                command = CommandList[cc];
                $.document.getElementById('current-command').textContent = e.target.dataset.command;
                $.runtime.sendCommandIfAllowed();
            }
        });
        $.document.getElementById('application-sidebar').appendChild(btn);
    });
    setInterval(function(){
        // send commands
        commandQueue.forEach(function(cmd){
            console.log('Trying send '+ cmd);
            var request = new XMLHttpRequest();
            request.open('POST', APPLICATION_WS + 'command');
            request.setRequestHeader('Application-ID', $.Application.id);
            request.setRequestHeader('RCP-Proto-Ver', RCP_PROTO_VER);
            request.onload = function(){
                var response = request.responseText.trim(),
                    idx = commandQueue.indexOf(response);
                console.log(response, cmd);
                if (idx != -1) {
                    commandQueue.splice(idx, 1);
                }
            };
            request.send(cmd);
        });
    }, 60000);
    setInterval(function(){
        if (lastSignal < ((new Date()).getTime() - 40000)) {
            $.runtime.resetState();
        }
    }, 10000);
    var tabs = $.document.querySelectorAll('[data-action]');
    for(var itr = 0; itr < tabs.length; itr++) {
        tabs.item(itr).addEventListener('click', function(event){
            var i = 0;
            for(i = 0; i < tabs.length; i++) {
                tabs.item(i).className = '';
                $.document.getElementById(tabs.item(i).dataset.action).className = 'hidden';
            }
            event.target.className = 'active';
            $.runtime.activeTab = event.target.dataset.action;
            $.document.getElementById(event.target.dataset.action).className = '';
            $.runtime.resetState();
        });
    }
    $.runtime.resetState();
    // initialize gantt
    gantt = new $.gantt($.document.getElementById('gantt'));
    gantt.setScale(60);
    $.runtime.updateGantt();
    setInterval(function(){
        $.runtime.updateGantt();
    }, 300000);
    setInterval(function(){
        var date = new Date();
        $.document.getElementById('clock').textContent = date.getHours() + ':' + (date.getMinutes() < 10 ? ('0' + date.getMinutes().toString()) : date.getMinutes());
    }, 1000);
})(window);